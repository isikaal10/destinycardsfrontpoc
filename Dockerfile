FROM nginx:alpine

COPY css /usr/share/nginx/html/css
COPY img /usr/share/nginx/html/img
COPY index.html /usr/share/nginx/html/
COPY js /usr/share/nginx/html/js
